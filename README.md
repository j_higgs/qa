Coding exercise for QA position
Expected result:
- If you choose to write code for the problems below, please create a bitbucket repository and share it with (​anoush@arkpes.com​, ​xin@arkpes.com​, ​rrusso@arkpes.com​, cflora@arkpes.com and ​alex@arkpes.com​) a day before scheduled interviews. Ideally before 3:00pm so that the team can have time to take a look.
- If you are going to use another tool please be prepared to demo it during a group interview.
We expect you would spend an hour or two on those problems. If you are not familiar with the technology, do your best to research it and figure out how to do it. If anything is not clear feel free to email to ​anoush@arkpes.com​. If you are making assumptions please document them.
Have fun!

## Problem 1:
This test will verify your Selenium knowledge, please be prepared to demo at the group interview
- Go to ​https://www.allrecipes.com
- Automate the following workflow with Selenium
- Navigate to signup page
- Register user with invalid password, one that doesn’t follow the rules of its creation
- Validate error message
- What other validations can you do on this page?
- Automate those steps.

## Solution
python, py test, and seleniumBase for webdriver framework
All the ui test work is done in ```./ui-tests/signupForm/validations.py``` 

## Install help 
- python (https://www.python.org/downloads/)
- git command line (https://git-scm.com/)
- python package installer helper (https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/)
- upgrade pip to remove possible warnings: ```python -m pip install -U pip```
- webdriver installed for which browsers to run tests against such as chromedriver (default)
- You can install seleniumBase via pip to help if you don't have a selenium webdriver installed: ```pip install seleniumbase```
- then install chromedriver: ```seleniumbase install chromedriver```

### Run ui tests
```
pytest ui-tests/signupForm/validations.py
```
_Incognito and with no cache:_
```
pytest ui-tests/signupForm/validations.py --incognito --crumbs`
```

_You can also slow them down and highlight by specifying the --demo option_
```
pytest ui-tests/signupForm/validations.py --demo
```
*Note there are two failing test cases which I left on purpose that look like bugs / confusing form validation alerts that should get attention on

There are some tests that are left failing because there were two cases the validation seems to be incorrect or needs to be updated.
1. Display names were able to made with ' ' spaces in them
2. Special characters were able to be used in the display name, for example $ and (

### Run ui test and generate report
```
nosetests ui-tests/signupForm/validations.py --report
```
_(report goes to ./latest_report/report.html)_

## Problem 2
This problem would test your knowledge on how to test REST APIs. Create an account at:
https://gorest.co.in/
Once you register you can get Authorization bearer token from here:
https://gorest.co.in/rest-console.html
Then use a tool of your choice to issue GET call to retrieve list of users whose first name is ​john​: https://gorest.co.in/public-api/users?first_name=john
Automate the following: Positive test
- Confirm that response code is 200
- Confirm that there are 3 users total
Negative test:
- Take out Authorization header and confirm expected response codes
Extra credit
- Read dynamically first user ID returned from previous response and pass it to the next GET call:
- GET https://gorest.co.in/public-api/users/<id>
- Validate returned userID matches the one passed

## Solution
All api tests are written in postman json format.  Collections and the environment file can be found in the api-tests directory

## For GUI Postman App
- Download the postman client https://www.postman.com/downloads/
- Import the collection file in ./api-tests/qa.postman_collection.json
- Import the global env file ./api-tests/qa.postman_environment.json (in the top right of postman GUI don't forget to select it after importing when you run in the GUI) 
- Run tests from the collection navigation

## Running Tests in GUI
- Make sure you have the environment imported and selected (./api-testsqa.postman_environment.json)
- Can run individually or using the test runner

## Running Tests in CLI
- First install newman globally  npm install -g newman
- can check version newman --version (latest 5.1.2)
- Then run the collection with newman cli: newman run ./api-tests/qa.postman_collection.json  -e ./api-tests/qa.postman_environment.json

## Optional newman reporter for api
- npm install -g newman-reporter-html
- Now you can add the reporter option along with running the tests to generate HTML report: newman run ./api-tests/qa.postman_collection.json  -e ./api-tests/qa.postman_environment.json -r html
- Reports will be generated under the ./newman directory